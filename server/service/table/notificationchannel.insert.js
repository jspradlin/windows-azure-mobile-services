function insert(item, user, request) {
    
    // Find the member id of the current user 
    // and attach it to the item before saving
   console.log('inserting new channel %j', item);
   
   var memberTable = tables.getTable('Member'); 
                    
	// Get the member record
    memberTable
       .where({ userId: user.userId})
       .take(1)
        .read({
            success: function(results){                
                if(results.length > 0){
                    // Capture the current user's member
                    var member = results[0];
                    
                    item.memberId = member.id; 
                    
                    // Ensure that the channel uri and memberId are unique
                    var channelTable = tables.getTable('NotificationChannel');
                    channelTable
                        .where({ uri: item.uri, memberId: member.id })
                        .read({ success: insertChannelIfNotFound });
                }else{
                    // A member was not found for this userId
                    // User has no posted to the userInfo custom api yet
                    request.respond(statusCodes.FORBIDDEN);
                    console.log('User attempted to insert notification without a member record. UserId:' + user.userId);
                }
            }
        });
        
    function insertChannelIfNotFound(existingChannels) {        
        if (existingChannels.length > 0) {
            console.log('channel already existed %j', item); 
            request.respond(200, existingChannels[0]);
        } else {
            console.log('inserting new channel %j',item); 
            request.execute();
        }
    }

}