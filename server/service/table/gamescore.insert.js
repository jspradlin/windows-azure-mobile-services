function insert(item, user, request) {

    // Ensure that the current user is a judge
   var memberTable = tables.getTable('Member'); 
                    
	// Get the member record
    memberTable
       .where({ userId: user.userId})
       .take(1)
        .read({
            success: function(results){
                if(results.length > 0){
                    // Capture the current user's member
                    var member = results[0];
                    
                    // If the person is a judge, include the id of the member on the record
                    if(member.isJudge){
                        // Add the judge id
                        item.judgeId = member.id; 
                        // Capture the createdDate
                        item.createdDate = new Date(); 
                        
                        // Allow the insert to continue
                        request.execute({
                            success: function(){
                                request.respond();
                                processNotifications(); 
                            }
                        })
                    }else{
                        // This person is not a judge
                        request.respond(statusCodes.FORBIDDEN);
                    }
                }else{
                    // A member was not found for this userId
                    // User has no posted to the userInfo custom api yet
                    request.respond(statusCodes.FORBIDDEN);
                }
            }, error: function(error){
                // Something happened trying to run a sql query
                request.respond(statusCodes.INTERNAL_SERVER_ERROR, { error: error});
            }
        });
        
    function processNotifications() {
        // Get the leaderbord results to display
        var sql = 
        'SELECT TOP 3 userDisplayName, ' +
        'overallRanking  FROM games_jts_1.vwOverallLeaderBoard ' +
        'ORDER BY OverallRanking, UserDisplayName '
        
        mssql.query(sql, null,{
            success: function(results){
                // Build the message for the push notification
                var backText = '';
                results.forEach(function(leader){
                    backText += leader.overallRanking + ' - ' + leader.userDisplayName  + '\r\n'; 
                });  
                // Send the messages 
                sendNotifications(backText, 'Leaders');
            }
        });        
    }
    
    function sendNotifications(backContent, backTitle){
        var channelTable = tables.getTable('NotificationChannel');
        channelTable.read({
            success: function(channels) {
                channels.forEach(function(channel) {
                    push.mpns.sendFlipTile(channel.uri, {
                        backTitle: backTitle,
                        backContent: backContent
                    }, {
                        success: function(pushResponse) {
                            console.log("Sent push:", pushResponse);
                        }
                    });
                });
            }
        });
    }

}