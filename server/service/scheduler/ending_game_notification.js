function ending_game_notification() {
    // Get a list of members that have not yet played a game and their channel information
    // Send push toast notifications to all users that have not played the game
    // Get the games that are expring 15 minutes from now
    var fifteenMinutesFromNow = new Date(new Date().getTime() + 15*60000);
    var sql = 'SELECT gameId, title, endTime, uri ' +
        'FROM games_jts_1.vwNonPlayedGamesNotifications ' +
        'WHERE endTime <= ?';
    var params = [fifteenMinutesFromNow]; 
    mssql.query(sql, params, {
        success: function(results){
            console.log('Notifying users of ending game. %j', results);
            results.forEach(notifyUserGameIsEnding); 
        }
    });
    
    function notifyUserGameIsEnding(data){
        // Send a toast notification to the specified uri
        push.mpns.sendToast(data.uri, {
            text1: data.title + ' is ending'
        }, {
            success: function(pushResponse) {
                // Log the response of the push notification
                console.log("Sent push:", pushResponse);
            }
        });
    }
}