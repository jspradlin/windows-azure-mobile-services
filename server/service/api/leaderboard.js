exports.get = function(request, response) {
    
    // Execute a query against MSSQL to get the game results
    
    // The optional parameters memberId == Members.Id and gameId are supplied
    // If no memberId or gameId is supplied, return the overall leaderboard
    // else, return the results filtered by the gameId and memberId
     
    var sql = 
        'SELECT MemberId, UserId, GameId, GameTitle, UserProfileImageUrl, UserDisplayName, ' +
        'Score, Ranking FROM games_jts_1.vwLeaderBoard '; 
    
    var params = [];

    if (request.query.memberId)
    {
        sql += ' where MemberId = ?';    
        params.push(request.query.memberId);
    }
    if (request.query.gameId)
    {
        sql += ' where GameId = ?';
        params.push(request.query.gameId);
    }
    
    sql +=  ' ORDER BY GameId, Ranking, UserDisplayName';
    
    request.service.mssql.query(sql, params,
        {
            success: function(results)
            {
                response.json(statusCodes.OK, results);
            },
            error : function(error)
            {
                console.error(error); 
                response.send(statusCodes.INTERNAL_SERVER_ERROR);
            }
        }
    );
};