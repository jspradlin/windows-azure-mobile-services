exports.get = function(request, response){
    var memberTable = request.service.tables.getTable('Member'); 
                    
	// Get the member record
    memberTable
       .where({ userId: request.user.userId})
       .take(1)
        .read({
            success: function(results){
                if(results.length > 0){
                    var member = results[0];
                    response.send(statusCodes.OK, member); 
                }else{
                    request.respond(statusCodes.NOT_FOUND);
                }
            }, error: function(error){
                request.respond(statusCodes.INTERNAL_SERVER_ERROR, error);
            }
        });
}

exports.post = function(request, response) {
        
    var identities = request.user.getIdentities();
    
    var basicProfileUrl;
    if (identities.microsoft) {
        var liveAccessToken = identities.microsoft.accessToken;
        basicProfileUrl = 'https://apis.live.net/v5.0/me/?method=GET&access_token=' + liveAccessToken;
    }
 
    if (basicProfileUrl) {
        
        var requestCallback = function (err, resp, body) {
            if (err || resp.statusCode !== 200) {
                console.error('Error getting basic profile data from the identity provider: ', err);
                request.respond(statusCodes.INTERNAL_SERVER_ERROR, body);
            } else {
                try {
                    
                    // Get the user data
                    var userData = JSON.parse(body);
                    console.log("Received data: %j", userData); 
                    
                    // Insert/Update data into the Member table
                    var memberTable = request.service.tables.getTable('Member'); 
                    
					// Get the member record
                    memberTable
                        .where({ userId: request.user.userId})
                        .take(1)
                        .read({
                            success: function(results){
                                
                                // Grab the existing data, else create a new one
                                var member = null;  
                                var profileImageUrl = "https://apis.live.net/v5.0/" + userData.id + "/picture"; 
                                
                                if(results.length > 0){
                                    // Update the user record
                                   member = results[0]; 
                                                                        
                                    // Apply the values from the userData to the member
                                    member.displayName = userData.name;
                                    member.firstName = userData.first_name;
                                    member.lastName = userData.last_name;
                                    member.profileImageUrl = profileImageUrl; 
                                    
                                    // Persist the updated member object
                                    console.log('Trying to update member: %j', member); 
                                    memberTable.update(member, {
                                        success: function(){
                                            response.send(statusCodes.OK, { message: 'successfully updated member'});
                                        }, 
                                        error: function(err){
                                            request.respond(statusCodes.INTERNAL_SERVER_ERROR, err);
                                        }
                                    });
                                }else{
                                     member = { 
                                        userId: request.user.userId, 
                                        displayName: userData.name,
                                        firstName: userData.first_name,
                                        lastName: userData.last_name,
                                        profileImageUrl: profileImageUrl,
                                        isJudge: false
                                    };
                                    
                                    // Persist the new member object
                                    console.log('Trying to insert member: %j', member); 
                                    memberTable.insert(member, {
                                        success: function(){
                                            response.send(statusCodes.OK, { message: 'successfully inserted member'});
                                        }, 
                                        error: function(err){
                                            request.respond(statusCodes.INTERNAL_SERVER_ERROR, err);
                                        }
                                    });  
                                }
                            }, 
                            error: function(err){
                                console.log('Error reading Member table. ' + err); 
                                request.respond(statusCodes.INTERNAL_SERVER_ERROR, err);
                            }
                        }); 
                    
                } catch (ex) {
                    console.error('Error parsing response from the provider API: ', ex);
                    request.respond(statusCodes.INTERNAL_SERVER_ERROR, ex);
                }
            }
        }
        
        // Get the request module
        var req = require('request');
        
        // Build the http request to get user data
        var reqOptions = {
            uri: basicProfileUrl,
            headers: { Accept: "application/json" }
        };
        
        // Perform the request for the user data        
        req(reqOptions, requestCallback);
        
    }else{
        response.send(statusCodes.OK, { message : 'No data update. Provider url was not found.' });
    }
};
