exports.get = function(request, response) {
    // Execute a query against MSSQL to get the leaderbord data    
    var sql = 'SELECT MemberId, UserId, UserProfileImageUrl, UserDisplayName, ' +
        'OverallRanking  FROM games_jts_1.vwOverallLeaderBoard '; 
        
    var params = [];
    // If the request includes a memberId include a where clause
    if (request.query.memberId){
        sql += ' where MemberId = ?';    
        params.push(request.query.memberId);
    }
    // Add the ordering
    sql += ' ORDER BY OverallRanking, UserDisplayName '
    
    // Execute the sql query and respond to the request
    // after the sql query is completed 
    request.service.mssql.query(sql, params, {
            success: function(results){
                response.json(statusCodes.OK, results);
            }
        }
    );  
};