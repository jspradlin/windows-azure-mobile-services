/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="Game.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using System;
using System.Runtime.Serialization;

namespace Games.Models
{
    [DataContract]
    public class Game
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Rules { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public DateTime StartTime { get; set; }

        [DataMember]
        public DateTime EndTime { get; set; }

        [DataMember]
        public bool TimedEvent { get; set; }

        [DataMember]
        public bool LowestScoreWins { get; set; }

    }
}
