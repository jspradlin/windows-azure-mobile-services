/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="MobileServiceClientExtensions.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
namespace Games.Web.Console.Extensions
{
    /// <summary>
    /// Extension methods for the MobileServiceClient class
    /// </summary>
    public static class MobileServiceClientExtensions
    {
        /// <summary>
        /// Add a FindRecord method to the client. 
        /// Adds generic access for finding a single record.
        /// </summary>
        public async static Task<T> FindRecord<T>(this MobileServiceClient client, Expression<Func<T, bool>> predicate)
        {
            var items = await client.GetTable<T>()
                .Where(predicate)
                .Take(1)
                .ToListAsync();

            return items.FirstOrDefault();
        }

        /// <summary>
        /// Add a FindRecords method to the client. 
        /// Adds generic access to get the contents of a table. 
        /// </summary>
        public async static Task<List<T>> FindRecords<T>(
            this MobileServiceClient client,
            Expression<Func<T, bool>> predicate = null)
        {
            var table = client.GetTable<T>();

            if (predicate != null)
            {
                table.Where(predicate);
            }

            return await table.ToListAsync();
        }

        /// <summary>
        /// Asynchronously retrieves ALL records from a table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <param name="bufferSize"></param>
        /// <returns></returns>
        public async static Task<List<T>> LoadAllAsync<T>(this IMobileServiceTableQuery<T> table, int bufferSize = 1000)
        {
            var query = table.IncludeTotalCount();
            var results = await query.ToEnumerableAsync();
            long count = ((ITotalCountProvider)results).TotalCount;
            if (results != null && count > 0)
            {
                var updates = new List<T>();
                while (updates.Count < count)
                {

                    var next = await query.Skip(updates.Count).Take(bufferSize).ToListAsync();
                    updates.AddRange(next);
                }
                return updates;
            }

            return null;
        }
    }
}