/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="MembersController.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Games.Web.Console.Models;
using Microsoft.WindowsAzure.MobileServices;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Games.Web.Console.Extensions; 

namespace Games.Web.Console.Controllers
{
    [Authorize()]
    public class MembersController : Controller
    {
        private MobileServiceClient mobileServiceClient =
            MobileServiceClientFactory.CreateClient();
        //
        // GET: /members/
        public async Task<ActionResult> Index()
        {
            var list = await mobileServiceClient.GetTable<Member>()
                .OrderBy(m => m.DisplayName)
                .LoadAllAsync(); 

            return View(list);
        }

        //
        // GET: /members/Details/5
        public async Task<ActionResult> Details(int id)
        {
            var member = await mobileServiceClient.FindRecord<Member>(m => m.Id == id); 
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }


        //
        // GET: /members/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var member = await mobileServiceClient.FindRecord<Member>(m => m.Id == id); 
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        //
        // POST: /members/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Member member)
        {
            if (ModelState.IsValid)
            {
                await mobileServiceClient.GetTable<Member>().UpdateAsync(member); 
                return RedirectToAction("Index");
            }
            return View(member);
        }

        //
        // GET: /members/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            var member = await mobileServiceClient.FindRecord<Member>(m => m.Id == id); 
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        //
        // POST: /members/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await mobileServiceClient.GetTable<Member>().DeleteAsync(new Member { Id = id }); 
            return RedirectToAction("Index");
        }

    }
}
