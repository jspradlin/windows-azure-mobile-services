/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="GamesController.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Games.Web.Console.Models;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Games.Web.Console.Controllers
{
    [Authorize]
    public class GamesController : Controller
    {
        private MobileServiceClient mobileServiceClient =
            MobileServiceClientFactory.CreateClient();

        //
        // GET: /Games/
        public async Task<ActionResult> Index()
        {
            var list = await mobileServiceClient.GetTable<Game>()
                .ToListAsync();
            return View(list);
        }

        //
        // GET: /Games/Details/5

        public async Task<ActionResult> Details(int id = 0)
        {
            var item = await FindGame(id); 
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        //
        // GET: /Games/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Games/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Game game)
        {
            if (ModelState.IsValid)
            {
                var games = mobileServiceClient.GetTable<Game>();
                await games.InsertAsync(game);
                return RedirectToAction("Index");
            }

            return View(game);
        }

        //
        // GET: /Games/Edit/5

        public async Task<ActionResult> Edit(int id = 0)
        {
            var game = await FindGame(id);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        //
        // POST: /Games/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Game game)
        {
            if (ModelState.IsValid)
            {
                await mobileServiceClient.GetTable<Game>()
                    .UpdateAsync(game);
                return RedirectToAction("Index");
            }
            return View(game);
        }

        //
        // GET: /Games/Delete/5

        public async Task<ActionResult> Delete(int id = 0)
        {
            var game = await FindGame(id);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        //
        // POST: /Games/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await mobileServiceClient.GetTable<Game>().DeleteAsync(new Game { Id = id });
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Helper method to find a game by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<Game> FindGame(int id)
        {
            var games = await mobileServiceClient.GetTable<Game>()
                 .Where(g => g.Id == id)
                 .Take(1)
                 .ToListAsync();

            return games.FirstOrDefault();
        }

    }
   
}
