﻿$(function () {
    $('.datetimepicker').datetimepicker({
        language: 'en',
        pick12HourFormat: true,
        pickTime: true
    });
});