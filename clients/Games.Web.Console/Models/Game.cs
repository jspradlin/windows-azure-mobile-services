/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="Game.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Games.Web.Console.Models
{
    [DataContract(Name="game")]
    public class Game
    {
        [DataMember]
        public int Id { get; set; }

        [Required]
        [DataMember]
        public string Title { get; set; }

        [Required]
        [DataMember]
        public string Rules { get; set; }

        [Required]
        [DataMember]
        public string Location { get; set; }

        [DataMember]
        [DataType(System.ComponentModel.DataAnnotations.DataType.DateTime)]
        [Display(Name="Starts At")]
        public DateTime StartTime { get; set; }

        [DataMember]
        [Display(Name="Ends At")]
        public DateTime EndTime { get; set; }

        [DataMember]
        [Display(Name="Is Timed")]
        public bool TimedEvent { get; set; }

        [DataMember]
        [Display(Name="Lower Is Better")]
        public bool LowestScoreWins { get; set; }

    }
}
