/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="MobileServiceClientFactory.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Microsoft.WindowsAzure.MobileServices;

namespace Games.Web.Console.Models
{
    public class MobileServiceClientFactory 
    {
        /// <summary>
        /// Builds an instance of the Mobile Services client. 
        /// </summary>
        /// <returns>MobileServiceClient</returns>
        public static MobileServiceClient CreateClient()
        {
            var mobileServiceClient =
            new MobileServiceClient("https://games-jts-1.azure-mobile.net/",
                "eyaxSIsCuYarnDPtqlLhNyTXnuCBHt16");

            // Enable camel case columns on the server
            mobileServiceClient.SerializerSettings.CamelCasePropertyNames = true;
            return mobileServiceClient; 
        }
        
    }
}
