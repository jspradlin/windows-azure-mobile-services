/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="Member.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Games.Web.Console.Models
{
     [DataContract(Name="member")]
    public class Member
    {
         [DataMember]
         public int Id { get; set; }
         
         [DataMember]
         [Display(Name="User Id")]
         public string UserId { get; set; }

         [DataMember]
         [Required]
         [Display(Name = "Display Name")]
         public string DisplayName { get; set; }

         [DataMember]
         [Required]
         [Display(Name = "First Name")]
         public string FirstName { get; set; }

         [DataMember]
         [Required]
         [Display(Name = "Last Name")]
         public string LastName { get; set; }

         [DataMember]
         [DataType(System.ComponentModel.DataAnnotations.DataType.ImageUrl)]
         [Display(Name="Profile Picture")]
         public string ProfileImageUrl { get; set; }

         [DataMember]
         [Display(Name="Is Judge")]
         public bool IsJudge { get; set; }
    }
}
