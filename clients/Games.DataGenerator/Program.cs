/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="Program.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using EfficientlyLazy.IdentityGenerator;
using Games.Models;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Games.DataGenerator
{
    class Program
    {
        private static Random randomGenerator = new Random(); 
        static void Main(string[] args)
        {
            GenerateMembers(); 

            Console.WriteLine("Finished, press any key to exit..."); 
            Console.Read(); 
        }

        private static void GenerateMembers()
        {
            var mobileService = CreateClient();
            var memberTable = mobileService.GetTable<Member>();

            // Generate 100 users
            const int totalGeneratedMembers = 50;
            for (var i = 0; i < totalGeneratedMembers; i++)
            {
                var generatedEntity = Generator.GenerateName(true, true);

                memberTable.InsertAsync(new Member
                {
                    DisplayName = string.Format("{0} {1}", generatedEntity.First, generatedEntity.Last),
                    FirstName = generatedEntity.First,
                    LastName = generatedEntity.Last,
                    IsJudge = false,
                    UserId = Guid.NewGuid().ToString(),
                    //ProfileImageUrl = string.Format("http://placehold.it/90x90&text={0}", generatedEntity.First)
                    ProfileImageUrl = GetProfileImage()
                }).Wait(); 


                Console.WriteLine(string.Format("Created {0} of {1}", i + 1, totalGeneratedMembers));
            }
        }

        /// <summary>
        /// Returns a random profile image url
        /// </summary>
        /// <returns></returns>
        private static string GetProfileImage()
        {
            var profileImages = new List<string>{
                "http://farm6.staticflickr.com/5446/9546230051_a191d883f1_q_d.jpg",
                "http://farm4.staticflickr.com/3765/9391011110_8f2d37a06f_q.jpg",
                "http://farm6.staticflickr.com/5339/9390886122_f44ddbac2b_q.jpg",
                "http://farm4.staticflickr.com/3737/9388109607_1b4ed9ac81_q.jpg",
                "http://farm4.staticflickr.com/3790/9202652061_78e0812bd2_q.jpg"
            };

            return profileImages[randomGenerator.Next(0, profileImages.Count - 1)]; 
        }

        /// <summary>
        /// Builds an instance of the Mobile Services client. 
        /// </summary>
        /// <returns></returns>
        public static MobileServiceClient CreateClient()
        {
            var mobileServiceClient =
            new MobileServiceClient("https://games-jts-1.azure-mobile.net/",
                "eyaxSIsCuYarnDPtqlLhNyTXnuCBHt16");

            mobileServiceClient.SerializerSettings.CamelCasePropertyNames = true;

            return mobileServiceClient;
        }
    }
}
