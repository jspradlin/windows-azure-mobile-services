CREATE VIEW games_jts_1.vwOverallLeaderBoard AS
WITH OverAllScores AS
(
	SELECT
		MemberId
		, UserId
		, UserProfileImageUrl
		, UserDisplayName
		, AVG(Ranking) AS AverageRanking
	FROM
		games_jts_1.vwLeaderBoard lb
	GROUP BY
		MemberId
		, UserId
		, UserProfileImageUrl
		, UserDisplayName
)
, RankedOverallScores AS
(
	SELECT
		oas.AverageRanking
		, ROW_NUMBER() OVER(ORDER BY AVG(AverageRanking)) AS Sequence
	FROM
		OverAllScores oas
	GROUP BY
		oas.AverageRanking
)


SELECT
	ors.MemberId
	, ors.UserId
	, ors.UserProfileImageUrl
	, ors.UserDisplayName
	, ors.AverageRanking
	,roas.Sequence AS OverallRanking	
FROM
	OverAllScores ors
	INNER JOIN RankedOverallScores roas ON roas.AverageRanking = ors.AverageRanking