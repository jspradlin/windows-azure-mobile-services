SELECT
	MemberId
	, UserId
	, GameId
	, UserProfileImageUrl
	, UserDisplayName
	, Score
	, Ranking
FROM
	games_jts_1.vwLeaderBoard
ORDER BY
	GameId
	, Ranking