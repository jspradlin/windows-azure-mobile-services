
DECLARE @NOW DATETIME = GETDATE();
DECLARE @GameId INT; 
DECLARE @MemberId INT; 

DECLARE member_game_cursor CURSOR FOR
    SELECT 
		g.Id
		, m.Id
	FROM
		games_jts_1.Member m,
		games_jts_1.Game g
	WHERE
		m.IsJudge = 0
		AND NOT EXISTS
		(
			SELECT 1 FROM games_jts_1.GameScore gs WHERE gs.GameId = g.Id AND gs.MemberId = m.Id
		)
		
OPEN member_game_cursor
FETCH NEXT FROM member_game_cursor
INTO @GameId, @MemberId

WHILE @@FETCH_STATUS = 0
BEGIN

	INSERT INTO games_jts_1.GameScore(GameId, MemberId, JudgeId, CreatedDate, Score)
	VALUES(@GameId, @MemberId, -1, @Now, RAND(1)); 

	FETCH NEXT FROM member_game_cursor
	INTO @GameId, @MemberId
END



CLOSE member_game_cursor;
DEALLOCATE member_game_cursor; 