ALTER VIEW games_jts_1.vwNonPlayedGamesNotifications AS
WITH NonPlayedGames AS
(
	SELECT 
		g.Id AS GameId
		, m.Id AS MemberId
	FROM
		games_jts_1.Game g, 
		games_jts_1.Member m
	WHERE
		NOT EXISTS
		(
			SELECT 1
			FROM
				games_jts_1.GameScore gs
			WHERE
				gs.GameId = g.Id
				AND gs.MemberId = m.Id
		)
)
SELECT 
	 npg.GameId
	 , g.Title
	 , g.StartTime
	 , g.EndTime
	 , GETDATE() AS NOW
	 , npg.MemberId
	 , m.DisplayName
	 , nc.Uri
FROM
	NonPlayedGames npg
	INNER JOIN games_jts_1.Game g ON g.Id = npg.GameId
	INNER JOIN games_jts_1.Member m ON m.Id = npg.MemberId
	INNER JOIN games_jts_1.NotificationChannel nc ON nc.MemberId = npg.MemberId
WHERE
	-- Make sure the game is running
	g.StartTime <= GETDATE() AND g.EndTime >= GETDATE()

