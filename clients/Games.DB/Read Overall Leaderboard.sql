SELECT
	MemberId
	, UserId
	, UserProfileImageUrl
	, UserDisplayName
	, OverallRanking
FROM
	games_jts_1.vwOverallLeaderBoard
ORDER BY
	OverallRanking