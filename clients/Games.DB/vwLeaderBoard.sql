ALTER VIEW games_jts_1.vwLeaderBoard AS

WITH PossibleGames AS
(
	/* Get ALL possible game plays */
	SELECT 
		g.Id AS GameId
		, m.Id AS MemberId
	FROM
		games_jts_1.Member m,
		games_jts_1.Game g
	WHERE
		ISNULL(m.IsJudge, 0) = 0
)
, GamePlays AS
(
	/* Figure out what is the most recent play for the member */
	SELECT
		gs.Id
		, gs.GameId
		, gs.MemberId
		, gs.Score
		, gs.CreatedDate
		, ROW_NUMBER() OVER(PARTITION BY gs.MemberId, gs.GameId ORDER BY CreatedDate DESC) AS Sequence
	FROM
		games_jts_1.GameScore gs
)
, ScoreRanking AS
(
	/* Determine the rank of the score for the game  */
	SELECT
		g.Id AS GameId
		, play.Score
		, CASE WHEN g.LowestScoreWins = 1
			THEN ROW_NUMBER() OVER(PARTITION BY g.Id ORDER BY play.Score) 
			ELSE ROW_NUMBER() OVER(PARTITION By g.Id ORDER BY play.Score DESC)
			END AS Ranking
	FROM
		(
			SELECT DISTINCT
				p.GameId
				, p.Score
			FROM
				GamePlays p
			WHERE 
				p.Sequence = 1
			GROUP BY
				p.GameId
				, p.Score			
		) play
		INNER JOIN games_jts_1.Game g ON g.Id = play.GameId
)
, EventLeaderBoard AS
(
	SELECT
		possibleGame.GameId
		, possibleGame.MemberId
		, play.Score
		, ranking.Ranking
	FROM
		PossibleGames possibleGame
		LEFT JOIN GamePlays play ON possibleGame.GameId = play.GameId AND possibleGame.MemberId = play.MemberId AND play.Sequence = 1
		LEFT JOIN ScoreRanking ranking ON ranking.GameId = play.GameId AND ranking.Score = play.Score
)

SELECT
	mem.Id AS MemberId
	, mem.UserId
	, ranking.GameId
	, g.Title AS GameTitle
	, mem.ProfileImageUrl AS UserProfileImageUrl
	, mem.DisplayName AS UserDisplayName
	, Score
	, CASE WHEN Ranking IS NULL
		THEN (SELECT MAX(Ranking) + 1 FROM EventLeaderBoard elb WHERE elb.GameId = ranking.GameId)
		ELSE Ranking
		END AS Ranking
FROM
	EventLeaderBoard ranking
	LEFT JOIN games_jts_1.Member mem ON mem.Id = ranking.MemberId
	INNER JOIN games_jts_1.Game g ON g.Id = ranking.GameId


