/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="Bootstrapper.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Caliburn.Micro;
using Caliburn.Micro.BindableAppBar;
using Games.WP8.Services;
using Games.WP8.ViewModels;
using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Games.WP8
{
    public class Bootstrapper : PhoneBootstrapper
    {
        PhoneContainer container;

        protected override void Configure()
        {
            base.Configure();

            container = new PhoneContainer();

            // HACK - Prevent errors on the App.xaml
            if (RootFrame != null)
            {
                container.RegisterPhoneServices(RootFrame);
            }

            // Configure the per-request view models
            container.PerRequest<MainPageViewModel>();
            container.PerRequest<GamesListViewModel>();
            container.PerRequest<LeadersViewModel>();
            container.PerRequest<GameDetailsViewModel>();
            container.PerRequest<GameJudgingListViewModel>();
            container.PerRequest<GameJudgingViewModel>();
            container.PerRequest<PlayerDetailsViewModel>(); 

            // Register the singleton data service
            container.Singleton<DataService>();

            AddCustomConventions();
        }

        private void AddCustomConventions()
        {
            // Custom Conventions for Bindable App Bar
            ConventionManager.AddElementConvention<BindableAppBarButton>(Control.IsEnabledProperty, "DataContext", "Click");
            ConventionManager.AddElementConvention<BindableAppBarMenuItem>(Control.IsEnabledProperty, "DataContext", "Click");

            // Custom convenetions for AutoCompleteBox
            ConventionManager.AddElementConvention<AutoCompleteBox>(AutoCompleteBox.ItemsSourceProperty, "SelectedItem", "SelectionChanged")
                 .ApplyBinding = (viewModelType, path, property, element, convention) =>
                 {
                     ConventionManager.SetBindingWithoutBindingOverwrite(viewModelType, path, property, element, convention, convention.GetBindableProperty(element));
                     ConventionManager.ConfigureSelectedItem(element, AutoCompleteBox.SelectedItemProperty, viewModelType, path);
                     return true;
                 };

        }

        protected override object GetInstance(Type service, string key)
        {
            return container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            container.BuildUp(instance);
        }
    }
}
