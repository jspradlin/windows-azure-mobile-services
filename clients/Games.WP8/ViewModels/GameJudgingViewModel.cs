/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="GameJudgingViewModel.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Caliburn.Micro;
using Games.Models;
using Games.WP8.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;

namespace Games.WP8.ViewModels
{
    public class GameJudgingViewModel : Screen
    {
        private readonly DataService _dataService;
        private readonly INavigationService _navigationService; 

        #region Properties
        private int _gameId;
        public int GameId
        {
            get { return _gameId; }
            set
            {
                _gameId = value;
                NotifyOfPropertyChange(() => GameId);
            }
        }

        private string _gameTitle;
        public string GameTitle
        {
            get { return _gameTitle; }
            set
            {
                _gameTitle = value;
                NotifyOfPropertyChange(() => GameTitle);
            }
        }

        private string _gameRules;
        public string GameRules
        {
            get { return _gameRules; }
            set
            {
                _gameRules = value;
                NotifyOfPropertyChange(() => GameRules);
            }
        }

        private bool _gameIsTimed;
        public bool GameIsTimed
        {
            get { return _gameIsTimed; }
            set
            {
                _gameIsTimed = value;
                NotifyOfPropertyChange(() => GameIsTimed);
            }
        }

        private bool _gameIsPointsBased;
        public bool GameIsPointsBased
        {
            get { return _gameIsPointsBased; }
            set
            {
                _gameIsPointsBased = value;
                NotifyOfPropertyChange(() => GameIsPointsBased);
            }
        }

        private List<Member> _players;

        public List<Member> Players
        {
            get { return _players; }
            set
            {
                _players = value;
                NotifyOfPropertyChange(() => Players);
            }
        }

        private Member _selectedPlayer;

        public Member SelectedPlayer
        {
            get { return _selectedPlayer;  }
            set
            {
                _selectedPlayer = value;
                NotifyOfPropertyChange(() => SelectedPlayer);
                NotifyOfPropertyChange(() => CanSubmitScore); 
            }
        }

        private string _scoredPoints;

        public string ScoredPoints
        {
            get { return _scoredPoints; }
            set
            {
                _scoredPoints = value;
                NotifyOfPropertyChange(() => ScoredPoints);
                NotifyOfPropertyChange(() => CanSubmitScore);
            }
        }

        public bool CanSubmitScore
        {
            get 
            {
                decimal score;
                var scoreIsValid = !string.IsNullOrEmpty(ScoredPoints) && decimal.TryParse(ScoredPoints, out score); 
                return scoreIsValid && SelectedPlayer != null;  
            }
        }

        #endregion

        #region Constructor
        public GameJudgingViewModel(DataService dataService, INavigationService navigationService)
        {
            _dataService = dataService;
            _navigationService = navigationService; 
        } 
        #endregion

        protected override async void OnActivate()
        {
            base.OnActivate();

            // Get the game and set the properties of the view model
            var game = await _dataService.GetGame(GameId); 
            if (game != null)
            {
                GameTitle = game.Title;
                GameRules = game.Rules;
                GameIsPointsBased = !game.TimedEvent;
                GameIsTimed = game.TimedEvent; 
            }

            Players = await _dataService.GetPlayers(); 
        }


        public async void SubmitScore()
        {
            try
            {
                decimal score = 0;
                var scoreIsValid = !string.IsNullOrEmpty(ScoredPoints) && decimal.TryParse(ScoredPoints, out score);

                if (scoreIsValid && SelectedPlayer != null)
                {
                    if (score >= 0)
                    {
                        await _dataService.SubmitScore(GameId, SelectedPlayer.Id, score);

                        // Notify the user of the success
                        MessageBox.Show("Score was recorded.");

                        // Reset the UI
                        ScoredPoints =  null;
                        SelectedPlayer = null; 
                    }
                    else
                    {
                        MessageBox.Show("Scores cannot be negative values.");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Please specify a user and score first.");
                    return;
                }

            }catch(Exception ex)
            {
                MessageBox.Show("Something happend and score was not recorded.");
                
                if (Debugger.IsAttached)
                {
                    Debugger.Break(); 
                }
            }

        }
    }
}
