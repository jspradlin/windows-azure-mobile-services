/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="PlayerDetailsViewModel.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Caliburn.Micro;
using Games.Models;
using Games.WP8.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Games.WP8.ViewModels
{
    public class PlayerDetailsViewModel : Screen
    {
        private readonly DataService _dataService;

        #region Properties
        private int _memberId;

        public int MemberId
        {
            get { return _memberId;  }
            set
            {
                _memberId = value;
                NotifyOfPropertyChange(() => MemberId); 
            }
        }

        private string _memberFirstName;

        public string MemberFirstName
        {
            get { return _memberFirstName;  }
            set
            {
                _memberFirstName = value;
                NotifyOfPropertyChange(() => MemberFirstName); 
            }
        }

        private string _memberLastName;

        public string MemberLastName
        {
            get { return _memberLastName; }
            set
            {
                _memberLastName = value;
                NotifyOfPropertyChange(() => MemberLastName);
            }
        }


        private string _memberProfileImageUrl;

        public string MemberProfileImageUrl
        {
            get { return _memberProfileImageUrl; }
            set
            {
                _memberProfileImageUrl = value;
                NotifyOfPropertyChange(() => MemberProfileImageUrl);
            }
        }

        private bool _isPlayer;

        public bool IsPlayer
        {
            get { return _isPlayer;  }
            set
            {
                _isPlayer = value;
                NotifyOfPropertyChange(() => IsPlayer); 
            }
        }

        private List<GameResult> _gameResults;

        public List<GameResult> GameResults
        {
            get { return _gameResults; }
            set
            {
                _gameResults = value;
                NotifyOfPropertyChange(() => GameResults);
            }
        }

        private int _overallRanking;

        public int OverallRanking
        {
            get { return _overallRanking; }
            set
            {
                _overallRanking = value;
                NotifyOfPropertyChange(() => OverallRanking); 
            }
        }
        #endregion

        #region Constructors
        public PlayerDetailsViewModel(DataService dataService)
        {
            this._dataService = dataService;
        } 
        #endregion

        protected override async void OnActivate()
        {
            base.OnActivate();

            // Get the user details for the specified MemberId
            var member = await _dataService.GetMember(MemberId);

            if (member != null)
            {
                MemberFirstName = member.FirstName;
                MemberLastName = member.LastName; 
                MemberProfileImageUrl = member.ProfileImageUrl;

                IsPlayer = !member.IsJudge; 

                // Lookup game results for this member
                var results = await _dataService.GetMemberStanding(MemberId);
                GameResults = results.OrderBy(gr => gr.GameTitle).ToList();

                // Lookup the overall results for this member
                var overallRankings = await _dataService.GetOverallLeaderBoard(MemberId);
                var overallRanking = overallRankings.FirstOrDefault(); 
                if (overallRanking != null)
                {
                    OverallRanking = overallRanking.OverallRanking; 
                }
            }
        }
        
    }
}
