/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="LeadersViewModel.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Caliburn.Micro;
using Games.Models;
using Games.WP8.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Games.WP8.ViewModels
{
    public class LeadersViewModel : Screen
    {
        private readonly DataService _dataService;
        private readonly INavigationService _navigationService; 


        private List<LeaderBoardEntry> _leaders;
        public List<LeaderBoardEntry> Leaders
        {
            get { return _leaders;  }
            set
            {
                _leaders = value;
                NotifyOfPropertyChange(() => Leaders); 
            }
        }

        private LeaderBoardEntry _selectedLeader;

        public LeaderBoardEntry SelectedLeader 
        {
            get { return _selectedLeader;  }
            set
            {
                _selectedLeader = value;
                NotifyOfPropertyChange(() => SelectedLeader); 

                // Navigate to the Player details screen
                if (SelectedLeader != null)
                {
                    _navigationService.UriFor<PlayerDetailsViewModel>()
                        .WithParam(vm => vm.MemberId, SelectedLeader.MemberId)
                        .Navigate(); 
                }
            }
        }


        public LeadersViewModel(DataService dataService, INavigationService navigationService)
        {
            DisplayName = "Leader Board";

            this._dataService = dataService;
            this._navigationService = navigationService; 

        }

        protected override async void OnActivate()
        {
            base.OnActivate();

            // Request the leaderboard data from the data service
            var results = await _dataService.GetOverallLeaderBoard(); 

            // Update the leaders property
            Leaders = results.ToList(); 

        }
    }
}
