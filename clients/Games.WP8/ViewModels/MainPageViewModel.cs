/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="MainPageViewModel.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Caliburn.Micro;
using Games.Models;
using Games.WP8.Helpers;
using Games.WP8.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Games.WP8.ViewModels
{
    public class MainPageViewModel : Conductor<IScreen>.Collection.OneActive
    {
        private readonly DataService _dataService;
        private readonly GamesListViewModel _gamesViewModel;
        private readonly LeadersViewModel _leadersViewModel;
        private readonly GameJudgingListViewModel _judgingViewModel;
        private readonly PlayerDetailsViewModel _playerViewModel; 

        #region Properties
        private bool isLoginVisible;

        public bool IsLoginVisible
        {
            get { return isLoginVisible; }
            set
            {
                isLoginVisible = value;
                NotifyOfPropertyChange(() => IsLoginVisible);
            }
        }

        private bool isLogOutVisible;

        public bool IsLogOutVisible
        {
            get { return isLogOutVisible; }
            set
            {
                isLogOutVisible = value;
                NotifyOfPropertyChange(() => IsLogOutVisible);
            }
        }

        private bool isJudge;

        public bool IsJudge
        {
            get { return isJudge; }
            set
            {
                isJudge = value;
                NotifyOfPropertyChange(() => IsJudge);

                if (isJudge)
                {
                    Items.Add(_judgingViewModel);
                }
                else
                {
                    Items.Remove(_judgingViewModel);
                }
            }
        }

        private Member currentMember;

        public Member CurrentMember
        {
            get { return currentMember; }
            set
            {
                currentMember = value;
                NotifyOfPropertyChange(() => CurrentMember);

                if (currentMember != null)
                {
                    _playerViewModel.MemberId = currentMember.Id; 
                    Items.Add(_playerViewModel);
                }
                else
                {
                    Items.Remove(_playerViewModel);
                }
            }
        }
        #endregion

        #region Constructors
        public MainPageViewModel()
        {

        }
        public MainPageViewModel(DataService dataService, 
            GamesListViewModel gamesViewModel, 
            LeadersViewModel leadersViewModel, 
            GameJudgingListViewModel judgingViewModel, 
            PlayerDetailsViewModel playerDetailsViewModel)
        {
            this._dataService = dataService;
            this._gamesViewModel = gamesViewModel;
            this._leadersViewModel = leadersViewModel;
            this._judgingViewModel = judgingViewModel;
            this._playerViewModel = playerDetailsViewModel;


            this._playerViewModel.DisplayName = "My Profile"; 
        }
        #endregion

        protected override async void OnInitialize()
        {
            base.OnInitialize();

            // Always show the leader board and games list
            Items.Add(_leadersViewModel);
            Items.Add(_gamesViewModel);

            if (_dataService.IsAuthenticated)
            {
                IsLoginVisible = false;
                IsLogOutVisible = true;

                // Determine if the user is a judge to add the judging panel
                IsJudge = await _dataService.IsUserJudge();

                CurrentMember = await _dataService.GetMember(); 
            }
            else
            {
                IsLoginVisible = true;
                IsLogOutVisible = false;
            }

            // Set the active item to the leaderboard
            ActivateItem(_leadersViewModel);
        }

        public async Task Login()
        {
            await DataService.Login();

            // Check to see if the logged in user is a judge, if so, add the juding panel
            if (_dataService.IsAuthenticated)
            {
                // Show/Hide the login/logout controls
                IsLoginVisible = false;
                IsLogOutVisible = true;

                IsJudge = await _dataService.IsUserJudge();
                CurrentMember = await _dataService.GetMember(); 
            }
            else
            {
                IsLoginVisible = true;
                IsLogOutVisible = false;
                IsJudge = false; 
                CurrentMember = null;
            }

        }
        public void Logout()
        {
            DataService.Logout();

            IsLoginVisible = true;
            IsLogOutVisible = false;
            IsJudge = false;
            CurrentMember = null;
        }


    }
}
