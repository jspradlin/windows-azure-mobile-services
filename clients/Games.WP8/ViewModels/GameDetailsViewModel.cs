/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="GameDetailsViewModel.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Caliburn.Micro;
using Games.Models;
using Games.WP8.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Games.WP8.ViewModels
{
    public class GameDetailsViewModel : Screen
    {
        private readonly DataService _dataService;
        private readonly INavigationService _navigationService;

        #region Properties
        private int _gameId;
        public int GameId
        {
            get { return _gameId; }
            set
            {
                _gameId = value;
                NotifyOfPropertyChange(() => GameId);
            }
        }

        private string _gameTitle;
        public string GameTitle
        {
            get { return _gameTitle; }
            set
            {
                _gameTitle = value;
                NotifyOfPropertyChange(() => GameTitle);
            }
        }

        private string _gameRules;
        public string GameRules
        {
            get { return _gameRules; }
            set
            {
                _gameRules = value;
                NotifyOfPropertyChange(() => GameRules);
            }
        }

        private bool _canJudge;
        public bool CanJudge
        {
            get { return _canJudge; }
            set
            {
                _canJudge = value;
                NotifyOfPropertyChange(() => CanJudge);
            }
        }

        private string _gameLocation;

        public string GameLocation
        {
            get { return _gameLocation; }
            set
            {
                _gameLocation = value;
                NotifyOfPropertyChange(() => GameLocation);
            }
        }

        private DateTime _gameStartTime;

        public DateTime GameStartTime
        {
            get { return _gameStartTime; }
            set
            {
                _gameStartTime = value;
                NotifyOfPropertyChange(() => GameStartTime);
            }
        }


        private DateTime _gameEndTime;

        public DateTime GameEndTime
        {
            get { return _gameEndTime; }
            set
            {
                _gameEndTime = value;
                NotifyOfPropertyChange(() => GameEndTime);
            }
        }

        private List<GameResult> _gameLeaders;

        public List<GameResult> GameLeaders
        {
            get { return _gameLeaders; }
            set
            {
                _gameLeaders = value;
                NotifyOfPropertyChange(() => GameLeaders);
            }
        }

        private GameResult _selectedGameLeader;

        public GameResult SelectedGameLeader
        {
            get { return _selectedGameLeader; }
            set
            {
                _selectedGameLeader = value;
                NotifyOfPropertyChange(() => SelectedGameLeader);

                // Navigate to the Player details screen
                if (SelectedGameLeader != null)
                {
                    _navigationService.UriFor<PlayerDetailsViewModel>()
                        .WithParam(vm => vm.MemberId, SelectedGameLeader.MemberId)
                        .Navigate();
                }
            }
        }



        #endregion

        #region Constructor
        public GameDetailsViewModel(DataService dataService, INavigationService navigationService)
        {
            _dataService = dataService;
            _navigationService = navigationService;
        }
        #endregion

        protected override async void OnActivate()
        {
            base.OnActivate();

            // Get the game and set the properties of the view model
            var game = await _dataService.GetGame(GameId);

            if (game != null)
            {
                GameTitle = game.Title;
                GameRules = game.Rules;
                GameLocation = game.Location;
                GameStartTime = game.StartTime;
                GameEndTime = game.EndTime;

                // Get the top ten leaders for this game
                var leaders = await _dataService.GetLeaderBoardResults(GameId);
                GameLeaders = leaders.Take(10).ToList();

            }

            // Determine if the game is active and the user is a judge
            var now = DateTime.Now;
            CanJudge = game.StartTime <= now && game.EndTime >= now && await _dataService.IsUserJudge();
        }

        public void JudgeGame()
        {
            if (CanJudge)
            {
                _navigationService.UriFor<GameJudgingViewModel>()
                    .WithParam<int>(vm => vm.GameId, GameId)
                    .Navigate();
            }
        }


    }
}
