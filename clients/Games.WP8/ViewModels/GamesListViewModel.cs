/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="GamesListViewModel.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Caliburn.Micro;
using Games.Models;
using Games.WP8.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Games.WP8.ViewModels
{
    public class GamesListViewModel : Screen
    {
        private readonly INavigationService _navigationService;
        private readonly DataService _dataService; 

        private List<Game> _games;
       
        public List<Game> Games
        {
            get { return _games;  }
            set
            {
                _games = value;
                NotifyOfPropertyChange(() => Games); 
            }
        }

        private Game _selectedGame;

        public Game SelectedGame
        {
            get { return _selectedGame;  }
            set
            {
                _selectedGame = value;
                NotifyOfPropertyChange(() => SelectedGame);

                if (_selectedGame != null)
                {
                    // Navigate to the details page
                    _navigationService.UriFor<GameDetailsViewModel>()
                        .WithParam(g => g.GameId, value.Id)
                        .Navigate();
                }
            }
        }

        public GamesListViewModel(INavigationService navigationService, DataService dataService)
        {
            this._navigationService = navigationService;
            this._dataService = dataService; 

            DisplayName = "Games"; 
        }

        protected override async void OnActivate()
        {
            base.OnActivate();

            // Call the data service to get the games
            var games = await _dataService.GetGames(); 
            
            // Update the list of games
            Games = games; 

        }
    }
}
