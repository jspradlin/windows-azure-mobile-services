/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="App.xaml.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Resources;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Games.WP8.Resources;
using Games.WP8.ViewModels;
using Microsoft.WindowsAzure.MobileServices;
using Games.WP8.Helpers;

namespace Games.WP8
{
    public partial class App : Application
    {
        /// <summary>
        /// Provides easy access to the root frame of the Phone Application.
        /// </summary>
        /// <returns>The root frame of the Phone Application.</returns>
        public static PhoneApplicationFrame RootFrame { get; private set; }

        /// <summary>
        /// Constructor for the Application object.
        /// </summary>
        public App()
        {
            // Standard XAML initialization
            InitializeComponent();

            // Show graphics profiling information while debugging.
            if (Debugger.IsAttached)
            {
                // Display the current frame rate counters.
                //Application.Current.Host.Settings.EnableFrameRateCounter = true;

                // Show the areas of the app that are being redrawn in each frame.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode,
                // which shows areas of a page that are handed off to GPU with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;

                // Prevent the screen from turning off while under the debugger by disabling
                // the application's idle detection.
                // Caution:- Use this under debug mode only. Application that disables user idle detection will continue to run
                // and consume battery power when the user is not using the phone.
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }

        }
    }
}
