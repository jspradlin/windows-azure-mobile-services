/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="UserStorageHelper.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Games.Models;
using Games.WP8.Services;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Games.WP8.Helpers
{
    public class UserStorageHelper
    {
        private const string AUTH_FILE_NAME = "user.info";
        public static AuthInfo LoadAuthInfoFromCache()
        {
            // Retrieve the data from isolated storage
            var protectedBytes = StorageHelper.ReadBytesFromStorage(AUTH_FILE_NAME);

            // If nothing was returned, then there is nothing else to parse
            if (protectedBytes == null) { return null; }

            // Decrypt the data
            var jsonAuthInfo = StorageHelper.UnprotectBytes(protectedBytes);

            // Deserialize into an object
            var authInfo = string.IsNullOrEmpty(jsonAuthInfo) ? null : JsonConvert.DeserializeObject<AuthInfo>(jsonAuthInfo);

            // return the results
            return authInfo;
        }

        /// <summary>
        /// Stores the supplied AuthInfo object in local storage as a encrypted json string.
        /// </summary>
        /// <param name="authInfo"></param>
        public static void StoreAuthInfoToCache(AuthInfo authInfo)
        {
            // JSON serialize the object
            var jsonString = JsonConvert.SerializeObject(authInfo);

            // Encrypt the data
            var protectedBytes = StorageHelper.GetProtectedBytes(jsonString);

            // Write to isolated storage
            StorageHelper.WriteBytesToStorage(protectedBytes, AUTH_FILE_NAME);
        }

        /// <summary>
        /// Method to overwrite the Auth Info from local storage
        /// </summary>
        public static void ClearAuthInfoFromCache()
        {
            StoreAuthInfoToCache(null); 
        }


    }
}
