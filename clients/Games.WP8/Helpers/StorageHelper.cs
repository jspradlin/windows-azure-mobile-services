/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="StorageHelper.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Games.WP8.Helpers
{
    public static class StorageHelper
    {
        public static byte[] ReadBytesFromStorage(string fileName)
        {
            byte[] encryptedBytes = null;

            try
            {
                // Access the file in the application's isolated storage.
                var file = IsolatedStorageFile.GetUserStoreForApplication();
                var readstream = new IsolatedStorageFileStream(fileName, System.IO.FileMode.Open, FileAccess.Read, file);

                // Read the data from the file.
                var reader = new StreamReader(readstream).BaseStream;
                encryptedBytes = new byte[reader.Length];

                reader.Read(encryptedBytes, 0, encryptedBytes.Length);
                reader.Close();
                readstream.Close();
            }
            catch (Exception ex)
            {
            }

            return encryptedBytes;
        }

        public static void WriteBytesToStorage(byte[] protectedBytes, string fileName)
        {
            // Create a file in the application's isolated storage.
            var file = IsolatedStorageFile.GetUserStoreForApplication();
            var writestream = new IsolatedStorageFileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write, file);

            // Write data to the file.
            var writer = new StreamWriter(writestream).BaseStream;
            writer.Write(protectedBytes, 0, protectedBytes.Length);
            writer.Close();
            writestream.Close();
        }

        public static byte[] GetProtectedBytes(string input)
        {
            if(input == null) return new byte[0];

            // Get the bytes for the input string
            var inputByptes = Encoding.UTF8.GetBytes(input);
            
            // Protect the bytes 
            var protectedBytes = ProtectedData.Protect(inputByptes, null);

            return protectedBytes; 
        }

        public static string UnprotectBytes(byte[] protectedBytes)
        {
            if (protectedBytes == null)
            {
                return null; 
            }
            
            // Get the unprotected bytes
            var unprotectedBytes = ProtectedData.Unprotect(protectedBytes, null);

            // Convert the byte[] to a UTF8 string
            string unprotectedString = null; 
            if (unprotectedBytes != null && unprotectedBytes.Length > 0)
            {
                unprotectedString = Encoding.UTF8.GetString(unprotectedBytes, 0, unprotectedBytes.Length);
            }
           
            // Return the UTF8 string
            return unprotectedString; 
        }
    }
}
