/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="AuthFilter.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Games.WP8.Services;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Games.WP8.Helpers
{
    /// <summary>
    /// Handles the auth token expiration. When the auth-token expires, 
    /// the user will receive a 401 request. This will trigger the
    /// login process and silently re-try the previously 401'd request
    /// </summary>
    public class AuthFilter : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);

            while (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                // The user was not authenticated - we got a 401 

                try
                {
                    // First try to login them in
                    await DataService.Login();
                }
                catch (InvalidOperationException ex)
                {
                    // User cancelled the login, return the original response
                    return response;
                }

                // Apply the new auth token since they logged in again
                request.Headers.Add("X-ZUMO-AUTH", DataService.MobileService.CurrentUser.MobileServiceAuthenticationToken);

                // Retry the request silently
                response = await base.SendAsync(request, cancellationToken);
            }


            return response;
        }
    }
}
