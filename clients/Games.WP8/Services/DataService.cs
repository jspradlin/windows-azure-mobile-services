/// <summary></summary>
///	<author>Justin Spradlin</author>
/// <requirements></requirements>
/// <date>9/9/2013 2:56:14 PM</date>
/// <copyright file="DataService.cs" company="Computer Technology Solutions, Inc">
/// 	Copyright (c) Computer Technology Solutions, Inc. ALL RIGHTS RESERVED 
///	</copyright>
/// <productName>CTS GAMES Demo</productName>
/// <remarks>
///		THE AUTHOR EXPRESSLY DISCLAIMS AND MAKES NO WARRANTIES, WHETHER WRITTEN OR ORAL, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, 
///		THE IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY.
///		IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR SPECIAL, CONSEQUENTIAL, INCIDENTAL, OR PUNITIVE DAMAGES, 
///		EITHER IN CONTRACT OR TORT, REGARDLESS OF WHETHER THE POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED TO THE 
///		AUTHOR IN ADVANCE OR COULD HAVE BEEN REASONABLY FORESEEN BY THE AUTHOR. IN NO EVENT SHALL THE AUTHORSíS LIABILITY, 
///		BY WAY OF DIRECT LIABILITY, INDEMNIFICATION, SUBROGATION, CONTRIBUTION, OR OTHERWISE, EXCEED THE FEES ACTUALLY PAID.
/// </remarks>

using Games.WP8.Helpers;
using Games.Models;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Phone.Notification;

namespace Games.WP8.Services
{
    public class DataService
    {
        /// <summary>
        /// Mobile Serivce referenced throughout the data access
        /// </summary>
        public static MobileServiceClient MobileService;


        /// <summary>
        /// Notification Channel to receive push notifications from mobile services
        /// </summary>
        public static HttpNotificationChannel CurrentChannel { get; private set; }

        /// <summary>
        /// Cached list of games
        /// </summary>
        private List<Game> games = null;


        #region Properties
        public bool IsAuthenticated
        {
            get
            {
                return MobileService != null && MobileService.CurrentUser != null;
            }
        }
        #endregion

        #region Constructor
        public DataService()
        {
            MobileService = new MobileServiceClient(
                // Url of the service
                "https://games-jts-1.azure-mobile.net/",
                // No - Auth Key - Since we are using authentication
                null,
                // Additional filters to intercept requests to the service
                // This AuthFilter will retry failed 401 requests due to 
                // expiration of the AuthToken from the logged in user
                new AuthFilter());

            // Set the serializer settings
            // This prevents having to set the json attributes or the [DataMember(Name="foo")] on data members
            MobileService.SerializerSettings.CamelCasePropertyNames = true;

            // Try to load the previously logged in user info
            RehydratePreviousUser();

        }
        #endregion

        #region Membership and Authentication
        /// <summary>
        /// Retrieves the previously saved user information from local storage. If a authentication information
        /// is retrieved, the mobile service is updated to use the last known user id and auth token.
        /// </summary>
        private void RehydratePreviousUser()
        {
            // Try to rehydrate the user from the cache
            var previousAuthInfo = UserStorageHelper.LoadAuthInfoFromCache();

            // If so, then create the new user and set the token
            // If any requests fail due to expired token, the AuthFilter.SendAsync will force the user to re-login
            if (previousAuthInfo != null &&
                !string.IsNullOrEmpty(previousAuthInfo.UserId) &&
                !string.IsNullOrEmpty(previousAuthInfo.AuthToken))
            {
                MobileService.CurrentUser = new MobileServiceUser(previousAuthInfo.UserId);
                MobileService.CurrentUser.MobileServiceAuthenticationToken = previousAuthInfo.AuthToken;

                // If the user was previously logged in the, they can get push notifications
                AquirePushChannel();
            }
        }

        public static async Task Login()
        {
            // If the token is not set, then login and capture the user id and auth token
            var user = await MobileService.LoginAsync(
                MobileServiceAuthenticationProvider.MicrosoftAccount);

            // If the user authenticated, encrypt and save the auth info
            if (user != null)
            {
                UserStorageHelper.StoreAuthInfoToCache(new AuthInfo
                {
                    UserId = user.UserId,
                    AuthToken = user.MobileServiceAuthenticationToken
                });

                // Invoke the Mobile Service API to create/refesh the member info 
                // based on the current user. If a member does not exist one 
                // will be created in the members table
                await MobileService.InvokeApiAsync("userinfo", HttpMethod.Post, null);

                // Since the user is logged in, get a push notification channel
                AquirePushChannel();
            }
        }

        public static void Logout()
        {
            // Log out of the mobile service
            MobileService.Logout();

            // Clear the stored user credentials
            UserStorageHelper.ClearAuthInfoFromCache();

        }

        /// <summary>
        /// Returns the Member record for the currently logged in user.
        /// </summary>
        /// <returns></returns>
        public async Task<Member> GetMember()
        {
            try
            {
                // Invoke the Mobile Service API to get the currently logged in user
                var results = await MobileService.InvokeApiAsync<Member>("userinfo", HttpMethod.Get, null);

                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Member> GetMember(int MemberId)
        {
            try
            {
                // Invoke the Mobile Service API to get the currently logged in user
                var results = await MobileService.GetTable<Member>()
                    .Where(m => m.Id == MemberId)
                    .Take(1)
                    .ToListAsync();

                return results.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns the members
        /// </summary>
        /// <returns></returns>
        public async Task<List<Member>> GetPlayers()
        {
            var players = await MobileService.GetTable<Member>()
                .Where(m => m.IsJudge == false)
                .ToListAsync();

            return players;
        }

        public async Task<bool> IsUserJudge()
        {
            // Do not make a trip to the mobile service if we know the user is not logged in
            if (!IsAuthenticated) return false;

            // Get the member's profile data
            var user = await GetMember();

            // Determine if the user is a judge to add the judging panel
            var isJudge = user != null && user.IsJudge;

            return isJudge;
        }
        #endregion

        #region Games and Scoring
        public async Task<List<Game>> GetGames()
        {
            games = await MobileService.GetTable<Game>().ToListAsync();
            return games.ToList();
        }

        /// <summary>
        /// Returns a list of games that are currently being played
        /// </summary>
        /// <returns></returns>
        public async Task<List<Game>> GetActiveGames()
        {
            DateTime now = DateTime.Now;

            var activeJudgingGames = await MobileService.GetTable<Game>()
               .Where(g => g.StartTime <= now && g.EndTime >= now)
               .ToListAsync();

            return activeJudgingGames;
        }

        public async Task<Game> GetGame(int id)
        {
            if (games == null)
            {
                await GetGames();
            }

            // Do not call mobile services on every call to get game
            // used the cached list of games instead
            //var games = await App.MobileService.GetTable<Game>()
            //    .Where(g => g.Id == id)
            //    .ToListAsync();

            return games.FirstOrDefault(g => g.Id == id);
        }

        public async Task<List<GameResult>> GetMemberStanding(int memberId)
        {
            return await GetLeaderBoardResults(null, memberId);
        }

        public async Task<List<GameResult>> GetGameStanding(int gameId)
        {
            return await GetLeaderBoardResults(gameId, null);
        }

        public async Task<List<GameResult>> GetLeaderBoardResults(int? gameId = default(int?), int? memberId = default(int?))
        {
            var parameters = new Dictionary<string, string>();

            if (gameId.HasValue)
            {
                parameters.Add("gameId", gameId.Value.ToString());
            }

            if (memberId.HasValue)
            {
                parameters.Add("memberId", memberId.ToString());
            }


            // Invoke the Mobile Service's leaderboard api to get the game results
            var results = await MobileService.InvokeApiAsync<List<GameResult>>(
                "leaderboard",
                HttpMethod.Get,
                parameters);

            return results;

        }

        public async Task<List<LeaderBoardEntry>> GetOverallLeaderBoard(int? memberId = default(int?))
        {
            var parameters = new Dictionary<string, string>();
            if (memberId.HasValue)
            {
                parameters.Add("memberId", memberId.Value.ToString()); 
            }
            var results = await MobileService.InvokeApiAsync<List<LeaderBoardEntry>>(
                "overallleaderboard"
                , HttpMethod.Get
                , parameters);

            return results;
        }

        public async Task SubmitScore(int gameId, int memberId, decimal score)
        {
            var gameScoreTable = MobileService.GetTable<GameScore>();

            var newScore = new GameScore
            {
                GameId = gameId,
                MemberId = memberId,
                Score = score
            };
            await gameScoreTable.InsertAsync(newScore);

        }
        #endregion

        #region Notifications
        /// <summary>
        /// Generates the Channel for Push notifications. This application 
        /// should only support push notifications for authenticated users. 
        /// This should be called when a user logs in or the 
        /// user is rehydrated from local storage.
        /// </summary>
        /// <returns></returns>
        private static void AquirePushChannel()
        {
            // Get the current channel
            CurrentChannel = HttpNotificationChannel.Find("MyPushChannel");

            // If a channel was not found create one
            if (CurrentChannel == null || CurrentChannel.ChannelUri == null || 
                string.IsNullOrEmpty(CurrentChannel.ChannelUri.ToString()))
            {
                CurrentChannel = new HttpNotificationChannel("MyPushChannel");
                CurrentChannel.Open();

                // Enable Flip Tile Notifications
                CurrentChannel.BindToShellTile();
                // Enable Toast Notifications in the notification bar
                CurrentChannel.BindToShellToast();
            }

            // Notify the mobile service of the current session's channel
            MobileService.GetTable<NotificationChannel>()
                .InsertAsync(new NotificationChannel 
                { 
                    Uri = CurrentChannel.ChannelUri.ToString() 
                });
        }

        /// <summary>
        /// Submits the current channel to the mobile service
        /// </summary>
        /// <returns></returns>
        private static void SubmitNotificationChannel()
        {
            MobileService.GetTable<NotificationChannel>()
                .InsertAsync(new NotificationChannel { Uri = CurrentChannel.ChannelUri.ToString() });

        }
        #endregion

    }
}
